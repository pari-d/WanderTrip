/**
 * Created by training on 3/29/2017.
 */

var module=angular.module('DateModule',['ngMaterial','ngMdIcons']);
module.controller('DateCtrl',['$scope',function($scope){
    $scope.dob='';
    $scope.currDate=new Date()
    $scope.maxdate=new Date(($scope.currDate.getYear()-21),1,1);
    $scope.mindate=new Date(1937,1,1)
}]);