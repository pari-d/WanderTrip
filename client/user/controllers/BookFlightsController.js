/**
 * Created by training on 3/28/2017.
 */
var app = angular.module('LoginModule',['DateModule', 'angAccordion','ngRoute','ngMaterial','ngAnimate','ngAria','ngMessages','ServiceModule','ngMdIcons'])

app.config(function ($routeProvider,$locationProvider) {
    $routeProvider.when('/homepage/:email',{
        templateUrl:'homepage.html',
        controller:'homeCtrl'
    })

})
app.controller('Ctrl',['$scope','loginService','$location', function($scope,loginService,$location,$route){
    $scope.currentPage=true;
    $scope.vm = {
        birthdate:"",
        name:"",
        username:"",
        email: "",
        password: "",
        gender: "",
        address:"",
        city:"",
        state:"",
        country:"",
        pin:"",

    };

    $scope.login=function () {
        console.log( $scope.vm );
        loginService.sendLoginDataObj( $scope.vm ).then(function (response) {
            console.log(response.data);
            if(response.data.search('Login')==-1){
                $scope.currentPage=false;
                $location.path('/homepage/'+$scope.vm.email);
            }
        })
    }
}]);

app.controller('homeCtrl',['$scope','$routeParams',function($scope, $routeParams){
    console.log($routeParams.email );
    $scope.username=$routeParams.email;
}]);

