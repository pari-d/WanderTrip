adminModule.controller('searchCtrl', ['$scope', function ($scope) {
    $scope.gridOptions={};
    
/*    var paginationOptions = {
    pageNumber: 1,
    pageSize: 25,
    sort: null
  };
   */
/*    $.routeProvider
    .when('/book',
         {templateUrl:'../views/review.html',controller:'reviewCtrl'})
})*/


    
$scope.gridOptions = {
        enableSorting: true,
       rowHeight:75,
        colHeight:75,
        paginationPageSize:0,
 
        columnDefs: [
        {name : 'Airline',field: 'Airline',headerCellClass:'head', cellTemplate:'<center style="margin-top:15px"><img class="img-rounded" width=\"40px\" height=\"40px\" ng-src=\"{{grid.getCellValue(row, col)}}\"><\center>', enableSorting: false},
            
        { name:'Flight Name', field: 'Flight-Name',headerCellClass:'head',headerTooltip: 'Custom header string',cellTemplate:'<center style="margin-top:25px;margin-left:5px;margin-right:5px">{{grid.getCellValue(row, col)}}</center>', enableSorting: false},
            
        {name:'Flight Number',field:'Flight-Number',headerCellClass:'head',cellTemplate:'<center style="margin-top:25px;margin-left:7px;margin-right:7px">{{grid.getCellValue(row, col)}}</center>', enableSorting: false},
            
        { name:'Departure Time', field: 'Departure-Time',headerCellClass:'head',cellTemplate:'<center style="margin-top:25px;margin-left:7px;margin-right:7px">{{grid.getCellValue(row, col)}}</center>', enableSorting: false},
            
        { name:'Arrival Time', field: 'Arrival-Time',headerCellClass:'head',cellTemplate:'<center style="margin-top:25px;margin-left:7px;margin-right:7px">{{grid.getCellValue(row, col)}}</center>', enableSorting: false}, 
            
        {name:'Duration',headerCellClass:'head',cellTemplate:'<center style="margin-top:25px;margin-left:7px;margin-right:7px">{{grid.getCellValue(row, col)}}</center>', enableSorting: false},
            
        {name:'Cost',headerCellClass:'head',cellTemplate:'<center style="margin-top:25px;margin-left:7px;margin-right:7px">{{grid.getCellValue(row, col)}}</center>'},
            
        { field: 'book', name: '', headerCellClass:'head',cellTemplate:'<center><md-button  class="md-primary md-raised md-button" id="btn"  ng-click="showConfirm($event,row)"  flex="100"  flex-gt-md="auto" style="background-color:steelblue;color:white;" size="16">   Book</md-button></center>', enableSorting: false}
        ],
        data : [      {
                           "Airline": "../../resources/images/logo_indigo.jpg",
                           "Flight-Name":"Indi987",
                           "Flight-Number":123987,
                           "Departure-Time":"11 hr 00 min",
                           "Arrival-Time" : "12 hr 30 min",
                           "Duration":"1 hr 30 min " ,
                           "Cost":"Rs.7300",
                       },
                
                       {
                           "Airline": "../../resources/images/logo_jetAirways.jpg",
                           "Flight-Name":"Jet9017",
                           "Flight-Number":127,
                           "Departure-Time":"19 hr 00 min",
                           "Arrival-Time" : "22 hr 30 min",
                           "Duration":"3 hr 30 min" ,
                           "Cost":"Rs.13000",
                       },
                       
                    
                       {
                           "Airline": "../../resources/images/logo_vistara.png",
                           "Flight-Name":"Vist712",
                           "Flight-Number":287,
                           "Departure-Time":"1 hr 00 min",
                           "Arrival-Time" : "7 hr 40 min",
                           "Duration":"7 hr 40 min" ,
                           "Cost":"Rs.8410",
                       },
                
                       {
                           "Airline": "../../resources/images/logo_spiceJet.png",
                           "Flight-Name":"spiceJet712",
                           "Flight-Number":287,
                           "Departure-Time":"1 hr 00 min",
                           "Arrival-Time" : "7 hr 40 min",
                           "Duration":"7 hr 40 min " ,
                           "Cost":"Rs.8410",
                       },
                
                   ]
      };
 
}]);