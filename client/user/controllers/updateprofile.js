 var app=angular.module('MyApp',['updateServiceModule','ngMaterial', 'ngMessages','ngRoute', 'material.svgAssetsCache'])
app.config(function($mdThemingProvider, $routeProvider) {

    // Configure a dark theme with primary foreground yellow

/*
    $mdThemingProvider.theme('docs-dark', 'default')
      .primaryPalette('yellow')
      .dark();
*/

    $routeProvider
      .when('/updateProfile', {templateUrl:'../views/updateprofile.html',controller:'AppCtrl'})
      .when('/viewProfile', {templateUrl:'../views/viewprofileang.html', controller:'viewProfileCtrl'})
      .otherwise({redirectTo:'/home', templateUrl:'../views/viewprofileang.html', controller:'viewProfileCtrl'})

  })
    
app.controller('DemoCtrl', function($scope,updateService) {
      $scope.user = {
        firstName: "",
        lastName: "",
        password:"",
        dob: "",
        email: "",
        contactNo: "",
        address: ""
  };

    $scope.states = ('AL AK AZ AR CA CO CT DE FL GA HI ID IL IN IA KS KY LA ME MD MA MI MN MS ' +
    'MO MT NE NV NH NJ NM NY NC ND OH OK OR PA RI SC SD TN TX UT VT VA WA WV WI ' +
    'WY').split(' ').map(function(state) {
        return {abbrev: state};
      });
  
   $scope.submit=function(){
        console.log($scope.user);
        updateService.addUpdateServiceObj($scope.user).then(function(response){
            console.log(response);
   })
                                                           }
})
  
/*
  .controller('AppCtrl', AppCtrl);

  function AppCtrl($scope) {
    $scope.currentNavItem = 'page1';
  }
*/

app.controller('AppCtrl',function($scope){
    console.log("AppCtrl")
     $scope.currentNavItem = 'page1';
    
})


