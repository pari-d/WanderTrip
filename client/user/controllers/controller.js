adminModule.config(function ($routeProvider,$locationProvider) {
    $routeProvider.when('/search',{
        templateUrl:'../views/searchResults.html',
        controller:'searchCtrl'
    })


});

adminModule.controller('stateCtrl',['$scope','viewstateService','$routeParams','webCheckinService','$mdDialog','$location',function($scope,viewstateService,$routeParams,webCheckinService,$mdDialog,$location){
    $scope.currentPage=true;
    $scope.dob='';
    $scope.selectedstate={};
    $scope.currDate=new Date();
    $scope.maxDate1=new Date(
        $scope.currDate.getFullYear(),
        $scope.currDate.getMonth()+3,
        $scope.currDate.getDate()
    )

    $scope.currVal=true;
    $scope.flag=true;

    $scope.toggle=function()
    {
        $scope.flag=false;
    }

    $scope.toggle1=function()
    {

        $scope.flag=true;
    }

    $scope.selectedstate={};
    viewstateService.viewstateServiceObj().then(function(response)
    {
        console.log(response);
        $scope.result=response;
        console.log(typeof($scope.result))

    });

    $scope.sendLogin=function()
    {
        $mdDialog.show({
            controller: regCtrl,
            templateUrl: 'login.html',
            parent: angular.element(document.body),
            //targetEvent: e,
            clickOutsideToClose:true,
            fullscreen: $scope.customFullscreen
            /*resolve: {
             data: function () {
             return row.entity;
             }
             }*/
        })
    }

    $scope.searchFlights=function()
    {

        if($scope.search!=-1)
        {
            $location.path('/search');
            $scope.currentPage=false;

        }
    };

    $scope.webCheckinFunction=function()
    {
        $mdDialog.show({
            controller: webChekinCtrl,
            templateUrl: 'webCheckin.html',
            parent: angular.element(document.body),
            //targetEvent: e,
            clickOutsideToClose:true,
            fullscreen: $scope.customFullscreen
            /*resolve: {
             data: function () {
             return row.entity;
             }
             }*/
        })
    };



    $scope.webCheckin={
        Pnr:"",
        Email:""};
    $scope.checkin=function(){

        /*console.log($scope.webCheckin.Pnr);
         console.log($scope.webCheckin.Email);*/
        console.log($scope.webCheckin);

        webCheckinService.webCheckinServiceObj($scope.webCheckin).then(function(response)
        {
            console.log(response);
            $scope.result=response;


        });
    }



}]);

function regCtrl($scope, $mdDialog) {

}
function loginCtrl($scope, $mdDialog) {
    console.log("test");

}
function webChekinCtrl($scope, $mdDialog) {

}
 

