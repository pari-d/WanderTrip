var serviceModule=angular.module('ServiceModule',[]);

serviceModule.service('viewstateService',['$http',function($http)//outer function
{
   var viewStateInfo=function(obj)//inner function
    {
       return $http(
       {
           method:'GET',
           datatype:"json",
           params:obj,//parameters that carries your data(object that we created we are sending it to server site)
           headers:{
               'Content-Type':'application/json'
           },
           url:'http://localhost:3000/viewcities'
       }
       ).then(function(response)
              {
               return response;//response will be returned to http and http will be returned to getstatelist
           
              })   
    }
   //CLOSURE single ton object returned from service(return data comes from getstatelist to service)
    return{
        viewstateServiceObj:viewStateInfo
    }
}])

serviceModule.service('regService',['$http',function($http){

    var addLoginInfo=function(obj)//stateobj selected from dropdown list
    {
        return $http({

            method:'POST',
            dataType:"json",
            params:obj,
            headers:{
                'Content-Type':'application/json'
            },
            url:'http://localhost:3000/registration'

        }).then(function(response){
            return response;
        })
    }

    return{
        addLoginServiceObj:addLoginInfo
    }
}])

/*serviceModule.service('loginService',['$http',function ($http) {
    var sendLoginData=function (obj) {
        return $http({
            method: 'POST',
            dataType: "json",
            params: obj,
            withCredentials:true,
            headers: {
                'Content-Type': 'application/json'
            },
            url: 'http://localhost:3000/login'
        }).then(function (response) {

            return response;
        })
    }
    return{
        sendLoginDataObj:sendLoginData
    }
}])*/

serviceModule.service('getLoginService',['$http',function($http){

    var  getLoginObj=function()
    {
        return $http({

            method:'POST',
            dataType:"json",
            headers:{
                'Content-Type':'application/x-www-form-urlencoded'
            },
            url:'http://localhost:3000/fetchData'
            //change url
        }).then(function(response){
            return response;

        })
    }

    return{
        loginServiceObj:getLoginObj
    }
}])


serviceModule.service('webCheckinService',['$http',function($http)//outer function
{
   var webCheckinserv=function(obj)//inner function
    {
       return $http(
       {
           method:'GET',
           datatype:"json",
           params:obj,//parameters that carries your data(object that we created we are sending it to server site)
           headers:{
               'Content-Type':'application/json'
           },
           url:'http://localhost:3000/webcheckin'
       }
       ).then(function(response)
              {
               return response;//response will be returned to http and http will be returned to getstatelist
           
              })   
    }
   //CLOSURE single ton object returned from service(return data comes from getstatelist to service)
    return{
        webCheckinServiceObj:webCheckinserv
    }
}])