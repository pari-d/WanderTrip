var updateServiceModule=angular.module('updateServiceModule',[]);


updateServiceModule.service('updateService',['$http',function($http){

    var addUpdateInfo=function(obj)//stateobj selected from dropdown list
    {
        return $http({

            method:'POST',
            dataType:"json",
            params:obj,
            headers:{
                'Content-Type':'application/json'
            },
            url:'http://localhost:3000/updateProfile'

        
        }).then(function(response){
            return response;
        })
    }

    return{
        addUpdateServiceObj:addUpdateInfo
    }
}])
