/*
adminModule.controller('stateCtrl',['$scope','stateService','$rootScope',function($scope,stateService,$rootScope)
{
    console.log('stateCtrl')

           
}]);*/
adminModule.controller('admin',function($scope){
    $scope.currentNavItem="page1"        
})
adminModule.config(function($routeProvider, $mdThemingProvider,$mdAriaProvider){
$routeProvider
        .when('/airline',{
        templateUrl:'partial/airline.html',
        controller:'airlineController'
        })
        .when('/flight',{
        templateUrl:'partial/flight.html',
        controller:'flightController'
        })
        .when('/city',{
        templateUrl:'partial/city.html',
        controller:'cityController' 
        })
        .when('/state',{
        templateUrl:'partial/state.html',
        controller:'stateController' 
        })
        .when('/route',{
        templateUrl:'partial/route.html',
        controller:'routeController'
        })
        .when('/airlineDetails',{
        templateUrl:'partial/airlinesData.html',
        controller:'airlinedetailCtrl'

        })
        .when('/flightDetails',{
        templateUrl:'partial/flightsData.html',
        controller:'flightdetailCtrl'

        })
        .when('/cityDetails',{
        templateUrl:'partial/cityData.html',
        controller:'citydetailCtrl'
        })
        .when('/stateDetails',{
        templateUrl:'partial/stateData.html',
        controller:'statedetailCtrl'
        })
        .when('/routeDetails',{
        templateUrl:'partial/routeData.html',
        controller:'routedetailCtrl'

        })
        .otherwise({redirectTo:'/airlineDetails'})
$mdThemingProvider.theme('default').accentPalette('blue');
$mdAriaProvider.disableWarnings();

})




    