adminModule.controller('stateController', ['$scope','$rootScope', 'uiGridConstants', function($scope, $rootScope, uiGridConstants){
            
             $scope.state={
                 StateCode:'',
                 StateName:'',
             }
            $scope.gridOptions = {};
            $scope.gridOptions = {
            enableFiltering: true,
            rowHeight:60,
            columnDefs: [
            { field: 'StateCode', width: 150 },
            { field: 'StateName' }
            ]
            };
            $scope.state = [
            {  StateCode:"BHR",StateName:"Bihar"},
            {  StateCode:"KRN",StateName:"Karnataka"},
            {  StateCode:"DEL",StateName:"Delhi"},
            {  StateCode:"AP",StateName:"Andhra Pradesh"},
            {  StateCode:"WB",StateName:"West Bengal"}
            ];
            $scope.getTableHeight = function() {
               var rowHeight = 60; // your row height
               var headerHeight = 60; // your header height
               return {
                  height: ($scope.gridOptions.data.length * rowHeight + headerHeight) + "px"
               };
            };
            $scope.gridOptions.data = $scope.state;
          //  $rootScope.gridOptions.columnDefs= $scope.gridOptions.columnDefs
            $scope.addNewItem=function()
            {
                 $scope.gridOptions.data.push( { Sno: 6, StateCode: $scope.state.StateCode , StateName:$scope.state.StateName});
            };  
        }])

adminModule.controller('statedetailCtrl',['$scope','$rootScope', 'uiGridConstants','getDataService',function($scope,$rootScope, uiGridConstants,getDataService){
    
     $rootScope.gridOptionsstate = {
            enableFiltering: true,
            rowHeight:60,
            columnDefs: [
            { field: 'StateCode' },
            { field: 'StateName' },
            { field: 'select', name: 'Edit', cellTemplate: 'modal-template-state.html', width: 120},
            { field: 'delete', name: 'Delete', cellTemplate: 'modal-template-delete-state.html', width: 120}
            ]
            };
    getDataService.serviceObj('state').then(function(response){
              
                  angular.forEach(response.data,function(value,key)
                  {
                      console.log(value);
                      $rootScope.gridOptionsstate.data.push( { StateCode: value.stateId, StateName:value.stateName });
                  })
                
            })
    
            $scope.state = [
            {  StateCode:"BHR",StateName:"Bihar"},
            {  StateCode:"KRN",StateName:"Karnataka"},
            {  StateCode:"DEL",StateName:"Delhi"},
            {  StateCode:"AP",StateName:"Andhra Pradesh"},
            {  StateCode:"WB",StateName:"West Bengal"}
            ];
            $scope.getTableHeight = function() {
               var rowHeight = 60; // your row height
               var headerHeight = 60; // your header height
               return {
                  height: ($rootScope.gridOptionsstate.data.length * rowHeight + headerHeight) + "px"
               };
            };
          //  $rootScope.gridOptionsstate.data = $scope.state;
          
}])
adminModule.directive('lazyModel', function($parse, $compile) {
  return {
    restrict: 'A',  
    require: '^form',
    scope: true,
    compile: function compile(elem, attr) {
        // getter and setter for original model
        var ngModelGet = $parse(attr.lazyModel);
        var ngModelSet = ngModelGet.assign;  
        // set ng-model to buffer in isolate scope
        elem.attr('ng-model', 'buffer');
        // remove lazy-model attribute to exclude recursion
        elem.removeAttr("lazy-model");
        return function postLink(scope, elem, attr) {
          // initialize buffer value as copy of original model 
          scope.buffer = ngModelGet(scope.$parent);
          // compile element with ng-model directive poining to buffer value   
          $compile(elem)(scope);
          // bind form submit to write back final value from buffer
          var form = elem.parent();
          while(form[0].tagName !== 'FORM') {
            form = form.parent();
          }
          form.bind('submit', function() {
            scope.$apply(function() {
                ngModelSet(scope.$parent, scope.buffer);
            });
         });
         form.bind('reset', function(e) {
            e.preventDefault();
            scope.$apply(function() {
                scope.buffer = ngModelGet(scope.$parent);
            });
         });
        };  
     }
  };
});
adminModule.controller('ModalCtrl5',['$scope','$mdDialog','$rootScope',function($scope,$mdDialog,$rootScope)
{
    $scope.open=function(e,row)
    {
       // console.log(row.entity);

        //in here, you can access the event object and row object
        var myEvent = e;
       $scope.currRow = row.entity;
   

            $mdDialog.show({
                controller: DialogController,
                templateUrl: 'edit-modal-state.html',
                parent: angular.element(document.body),
                targetEvent: e,
                clickOutsideToClose:true,
                fullscreen: $scope.customFullscreen,
                resolve: {
                    data: function () {
                        return row.entity;
                    }
                }
            })
                .then(function(answer) {
                    $scope.status = 'You said the information was "' + answer + '".';
                }, function() {
                    $scope.status = 'You cancelled the dialog.';
                })

    }
       
            $scope.showConfirm = function(event,row) {
               var confirm = $mdDialog.confirm()
                  .title('Are you sure to delete the record?')
                  .textContent('Record will be deleted permanently.')
                  .targetEvent(event)
                  .ok('Yes')
                  .cancel('No');
                  $mdDialog.show(confirm).then(function() {
                     console.log(row.entity );
                     var index = $rootScope.gridOptionsstate.data.indexOf(row.entity);
                    $rootScope.gridOptionsstate.data.splice(index, 1);
                     }, function() {
                        console.log('You decided to keep your record.');
                  });
            };
}])
function DialogController($scope, $mdDialog,data) {

      $scope.currency=data;

    $scope.hide = function() {
        $mdDialog.hide();
    };

    $scope.cancel = function() {
       // $scope.currency=data;
        $mdDialog.cancel();
    };

    $scope.answer = function(answer) {
        $mdDialog.hide(answer);
    };
}
