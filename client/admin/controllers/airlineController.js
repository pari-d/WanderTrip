  adminModule.controller('airlineController', ['$scope', 'uiGridConstants','setDataService', function($scope, uiGridConstants,setDataService){
             $scope.airline={
                 Code:'',
                 Name:'',
                 logo:''
             }
           // console.log(" airlineController")
            $scope.gridOptions = {};
            $scope.gridOptions = {
            enableFiltering: true,
            rowHeight:60,
            columnDefs: [
            { field: 'Code' },
            { field: 'Name' },
            {field: 'logo', cellTemplate:"<img width=\"50px\" ng-src=\"{{grid.getCellValue(row, col)}}\">" }
            ],
            onRegisterApi: function (gridApi) {
            $scope.grid1Api = gridApi;
            }
            };
      
            $scope.getTableHeight = function() {
               var rowHeight = 60; // your row height
               var headerHeight = 60; // your header height
               return {
                    height: ($scope.gridOptions.data.length * rowHeight + headerHeight) + "px"
               };
            };
        
            $scope.airlines = [
            { Code:"A100",Name: "Air India",logo:"http://lorempixel.com/20/20/sports/"},
            /*{  Code:"A200",Name: "Jet Airways",logo:"http://lorempixel.com/20/20/sports/"},
            {  Code:"A300",Name: "IndiGo",logo:"http://lorempixel.com/20/20/sports/" },
            {  Code:"A400",Name: "SpiceJet",logo:"http://lorempixel.com/20/20/sports/" },*/
            {  Code:"A500",Name: "GoAir",logo:"http://lorempixel.com/20/20/sports/" }
            ];
            $scope.gridOptions.data = $scope.airlines;
            $scope.addNewItem=function()
            {
                var obj={Code: $scope.airline.Code , Name:$scope.airline.Name, logo:"http://lorempixel.com/20/20/sports/"}
                $scope.gridOptions.data.push(obj);
                console.log(obj)
                setDataService.serviceObj(obj,'airline').then(function(response){
                    console.log(response)
                });
                    
            };   
            
        }])
        

adminModule.controller('airlinedetailCtrl',['$scope','$rootScope','getDataService', 'uiGridConstants', function($scope, $rootScope, getDataService, uiGridConstants){
    
   // $scope.airlinedata=[]
    $rootScope.gridOptionsairline = {
            enableFiltering: true,
           rowHeight:60,
            columnDefs: [
            { field: 'Code' },
            { field: 'Name' },
            {field: 'logo', cellTemplate:"<img width=\"50px\" ng-src=\"{{grid.getCellValue(row, col)}}\">" },
            { field: 'select', name: 'Edit', cellTemplate: 'modal-template-airline.html', width: 120},
            { field: 'delete', name: 'Delete', cellTemplate: 'modal-template-delete-airline.html', width: 120}
            ]
            };
            getDataService.serviceObj('airline').then(function(response){
                console.log(response.data)
               
                  angular.forEach(response.data,function(value,key)
                  {
                     // console.log(value);
                      $rootScope.gridOptionsairline.data.push( { Code: value.airlineId, Name:value.name, logo:value.image });
                  })
                
            })
           // $scope.airlinedata=JSON.parse($scope.airlinedata)
            /*for(var i=0;i< $scope.airlinedata.length;i++)
                {
                    console.log($scope.airlinedata[i].code)
                }*/
           /* for(var x in  $scope.airlinedata ){
                console.log(x)
            }*/
            $scope.airlines = [
            { Code:"A100",Name: "Air India",logo:"http://lorempixel.com/20/20/sports/"},
            {  Code:"A200",Name: "Jet Airways",logo:"http://lorempixel.com/20/20/sports/"},
            {  Code:"A300",Name: "IndiGo",logo:"http://lorempixel.com/20/20/sports/" },
            {  Code:"A400",Name: "SpiceJet",logo:"http://lorempixel.com/20/20/sports/" },
            {  Code:"A500",Name: "GoAir",logo:"http://lorempixel.com/20/20/sports/" }
            ];
            $scope.getTableHeight = function() {
               var rowHeight = 60; // your row height
               var headerHeight = 60; // your header height
               return {
                  height: ($rootScope.gridOptionsairline.data.length * rowHeight + headerHeight) + "px"
               };
            };
          // $rootScope.gridOptionsairline.data = $scope.airlines;
    
           
    
}])



adminModule.directive('lazyModel', function($parse, $compile) {
  return {
    restrict: 'A',  
    require: '^form',
    scope: true,
    compile: function compile(elem, attr) {
        // getter and setter for original model
        var ngModelGet = $parse(attr.lazyModel);
        var ngModelSet = ngModelGet.assign;  
        // set ng-model to buffer in isolate scope
        elem.attr('ng-model', 'buffer');
        // remove lazy-model attribute to exclude recursion
        elem.removeAttr("lazy-model");
        return function postLink(scope, elem, attr) {
          // initialize buffer value as copy of original model 
          scope.buffer = ngModelGet(scope.$parent);
          // compile element with ng-model directive poining to buffer value   
          $compile(elem)(scope);
          // bind form submit to write back final value from buffer
          var form = elem.parent();
          while(form[0].tagName !== 'FORM') {
            form = form.parent();
          }
          form.bind('submit', function() {
            scope.$apply(function() {
                ngModelSet(scope.$parent, scope.buffer);
            });
         });
         form.bind('reset', function(e) {
            e.preventDefault();
            scope.$apply(function() {
                scope.buffer = ngModelGet(scope.$parent);
            });
         });
        };  
     }
  };
});
adminModule.controller('ModalCtrl1',['$scope','$mdDialog','$rootScope',function($scope,$mdDialog,$rootScope)
{
    $scope.open=function(e,row)
    {
       // console.log(row.entity);

        //in here, you can access the event object and row object
        var myEvent = e;
       $scope.currRow = row.entity;
   

            $mdDialog.show({
                controller: DialogController,
                templateUrl: 'edit-modal-airline.html',
                parent: angular.element(document.body),
                targetEvent: e,
                clickOutsideToClose:true,
                fullscreen: $scope.customFullscreen,
                resolve: {
                    data: function () {
                        return row.entity;
                    }
                }
            })
                .then(function(answer) {
                    $scope.status = 'You said the information was "' + answer + '".';
                }, function() {
                    $scope.status = 'You cancelled the dialog.';
                })

    }
       
            $scope.showConfirm = function(event,row) {
               var confirm = $mdDialog.confirm()
                  .title('Are you sure to delete the record?')
                  .textContent('Record will be deleted permanently.')
                  .targetEvent(event)
                  .ok('Yes')
                  .cancel('No');
                  $mdDialog.show(confirm).then(function() {
                     console.log(row.entity );
                     var index = $rootScope.gridOptionsairline.data.indexOf(row.entity);
                    $rootScope.gridOptionsairline.data.splice(index, 1);
                     }, function() {
                        console.log('You decided to keep your record.');
                  });
            };
}])
function DialogController($scope, $mdDialog,data) {
    $scope.currency=data;
    $scope.hide = function() {
        $mdDialog.hide();
    };
    $scope.cancel = function() {
       // $scope.currency=data;
        $mdDialog.cancel();
    };

    $scope.answer = function(answer) {
        $mdDialog.hide(answer);
    };
}
