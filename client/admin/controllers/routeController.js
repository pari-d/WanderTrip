adminModule.controller('routeController',['$scope','$rootScope', 'uiGridConstants', function($scope,$rootScope, uiGridConstants){
            
             $scope.route={
                 RouteId:55,
                 SourceCity:'',
                 DestinationCity:'',
                 Distance:5
             }
            $scope.gridOptions = {};
            $scope.gridOptions = {
            enableFiltering: true,
            rowHeight:60,
            columnDefs: [
            { field: 'RouteId' },
            { field: 'SourceCity'},
            { field: 'DestinationCity' },
            { field: 'Distance' }
            ]
            };
            $scope.route = [
            { RouteId:"R100",SourceCity:"C100",DestinationCity: "C200",Distance:40},
            { RouteId:"R200",SourceCity:"C200",DestinationCity: "C400",Distance:50},
            { RouteId:"R300",SourceCity:"C300",DestinationCity: "C100",Distance:60},
            { RouteId:"R400",SourceCity:"C400",DestinationCity: "C300",Distance:40 },
            { RouteId:"R500",SourceCity:"C500",DestinationCity: "C500",Distance:40 }
            ];
            $scope.getTableHeight = function() {
               var rowHeight = 60; // your row height
               var headerHeight = 60; // your header height
               return {
                  height: (($scope.gridOptions.data.length + 1) * rowHeight + headerHeight) + "px"
               };
            };
            $scope.gridOptions.data = $scope.route;
          //  $rootScope.gridOptions.columnDefs= $scope.gridOptions.columnDefs
            $scope.addNewItem=function()
            {
                
distanceService.distanceServiceObject($scope.route.SourceCity,$scope.route.DestinationCity).then(function(response){
    
    return response;
    
                      });
                    
                 $scope.gridOptions.data.push( { Sno: 6, RouteId: $scope.route.RouteId , SourceCity:$scope.route.SourceCity, DestinationCity:$scope.route.DestinationCity,Distance: $scope.route.Distance});
            };  
        }])

adminModule.controller('routedetailCtrl',['$scope','$rootScope', 'uiGridConstants','getDataService',function($scope,$rootScope, uiGridConstants,getDataService){
    
     $rootScope.gridOptionsroute = {
            enableFiltering: true,
           rowHeight:60,
            columnDefs: [
            { field: 'RouteId' },
            { field: 'SourceCity' },
            { field: 'DestinationCity' },
            { field: 'Distance' },
            { field: 'select', name: 'Edit', cellTemplate: 'modal-template-route.html', width: 120},
            { field: 'delete', name: 'Delete', cellTemplate: 'modal-template-delete-route.html', width: 120}
                
            ]
            };
    
            getDataService.serviceObj('route').then(function(response){

              angular.forEach(response.data,function(value,key)
              {
                  
                  console.log(value);
                  $rootScope.gridOptionsroute.data.push( { RouteId: value.routeId, SourceCity:value.fromCity[0].cityName,DestinationCity:value.toCity[0].cityName ,Distance:value.distance});
              })

            })
    
    
            $scope.route = [
            { RouteId:"R100",SourceCity:"C100",DestinationCity: "C200",Distance:40},
            { RouteId:"R200",SourceCity:"C200",DestinationCity: "C400",Distance:50},
            { RouteId:"R300",SourceCity:"C300",DestinationCity: "C100",Distance:60},
            { RouteId:"R400",SourceCity:"C400",DestinationCity: "C300",Distance:40 },
            { RouteId:"R500",SourceCity:"C500",DestinationCity: "C500",Distance:40 }
            ];
            $scope.getTableHeight = function() {
               var rowHeight = 60; // your row height
               var headerHeight = 60; // your header height
               return {
                  height: (($rootScope.gridOptionsroute.data.length + 1) * rowHeight + headerHeight) + "px"
               };
            };
         //   $rootScope.gridOptionsroute.data = $scope.route;
}])
adminModule.directive('lazyModel', function($parse, $compile) {
  return {
    restrict: 'A',  
    require: '^form',
    scope: true,
    compile: function compile(elem, attr) {
        // getter and setter for original model
        var ngModelGet = $parse(attr.lazyModel);
        var ngModelSet = ngModelGet.assign;  
        // set ng-model to buffer in isolate scope
        elem.attr('ng-model', 'buffer');
        // remove lazy-model attribute to exclude recursion
        elem.removeAttr("lazy-model");
        return function postLink(scope, elem, attr) {
          // initialize buffer value as copy of original model 
          scope.buffer = ngModelGet(scope.$parent);
          // compile element with ng-model directive poining to buffer value   
          $compile(elem)(scope);
          // bind form submit to write back final value from buffer
          var form = elem.parent();
          while(form[0].tagName !== 'FORM') {
            form = form.parent();
          }
          form.bind('submit', function() {
            scope.$apply(function() {
                ngModelSet(scope.$parent, scope.buffer);
            });
         });
         form.bind('reset', function(e) {
            e.preventDefault();
            scope.$apply(function() {
                scope.buffer = ngModelGet(scope.$parent);
            });
         });
        };  
     }
  };
});
adminModule.controller('ModalCtrl4',['$scope','$mdDialog','$rootScope',function($scope,$mdDialog,$rootScope)
{
    $scope.open=function(e,row)
    {
       // console.log(row.entity);

        //in here, you can access the event object and row object
        var myEvent = e;
       $scope.currRow = row.entity;
   

            $mdDialog.show({
                controller: DialogController,
                templateUrl: 'edit-modal-route.html',
                parent: angular.element(document.body),
                targetEvent: e,
                clickOutsideToClose:true,
                fullscreen: $scope.customFullscreen,
                resolve: {
                    data: function () {
                        return row.entity;
                    }
                }
            })
                .then(function(answer) {
                    $scope.status = 'You said the information was "' + answer + '".';
                }, function() {
                    $scope.status = 'You cancelled the dialog.';
                })

    }
       
            $scope.showConfirm = function(event,row) {
               var confirm = $mdDialog.confirm()
                  .title('Are you sure to delete the record?')
                  .textContent('Record will be deleted permanently.')
                  .targetEvent(event)
                  .ok('Yes')
                  .cancel('No');
                  $mdDialog.show(confirm).then(function() {
                     console.log(row.entity );
                     var index = $rootScope.gridOptionsroute.data.indexOf(row.entity);
                    $rootScope.gridOptionsroute.data.splice(index, 1);
                     }, function() {
                        console.log('You decided to keep your record.');
                  });
            };
}])
function DialogController($scope, $mdDialog,data) {


      $scope.currency=data;

    $scope.hide = function() {
        $mdDialog.hide();
    };

    $scope.cancel = function() {
       // $scope.currency=data;
        $mdDialog.cancel();
    };

    $scope.answer = function(answer) {
        $mdDialog.hide(answer);
    };
}
