adminModule.controller('cityController', ['$scope','$rootScope', 'uiGridConstants',function($scope,$rootScope, uiGridConstants){
            
             $scope.city={
                 CityCode:'',
                 CityName:'',
                 AirportName:'',
                 StateId:5
             }
           
            $scope.gridOptions = {};
            $scope.gridOptions = {
            enableFiltering: true,
            rowHeight:60,
            columnDefs: [
            { field: 'CityCode'},
            { field: 'CityName'},
            { field: 'AirportName'},
            { field: 'StateId' }
            ]
            };
$scope.city = [
            {  CityCode:"C100",CityName:"Delhi",AirportName: "Delhi",StateId:40},
            {  CityCode:"C200",CityName:"Bangalore",AirportName: "Bangalore",StateId:50},
            {  CityCode:"C300",CityName:"Patna",AirportName: "Patna",StateId:60},
            {  CityCode:"C400",CityName:"Dehradun",AirportName: "Dehradun",StateId:40 },
            {  CityCode:"C500",CityName:"Mumbai",AirportName: "Mumbai",StateId:40 }
            ];
            $scope.getTableHeight = function() {
               var rowHeight = 60; // your row height
               var headerHeight = 60; // your header height
               return {
                  height: (($scope.gridOptions.data.length + 1) * rowHeight + headerHeight) + "px"
               };
            };
            $scope.gridOptions.data = $scope.city;
          //  $rootScope.gridOptions.columnDefs= $scope.gridOptions.columnDefs
            $scope.addNewItem=function()
            {
                 $scope.gridOptions.data.push( { CityCode: $scope.city.CityCode , CityName:$scope.city.CityName, AirportName:$scope.city.AirportName,StateId: $scope.city.StateId});
            };  
        }])

adminModule.controller('citydetailCtrl',['$scope','$rootScope', 'uiGridConstants','getDataService',function($scope,$rootScope, uiGridConstants,getDataService){
    
     $rootScope.gridOptionscity = {
            enableFiltering: true,
           rowHeight:60,
            columnDefs: [
            { field: 'CityCode' },
            { field: 'CityName' },
            { field: 'AirportName' },
            { field: 'StateId' },
            { field: 'select', name: 'Edit', cellTemplate: 'modal-template-city.html', width: 120},
            { field: 'delete', name: 'Delete', cellTemplate: 'modal-template-delete-city.html', width: 120}
            
                
           /* { field: 'Edit' , enableFiltering: false },
            { field: 'Delete' , enableFiltering: false }*/
                
            ]
            };
            getDataService.serviceObj('city').then(function(response){
              
               
                  angular.forEach(response.data,function(value,key)
                  {
                     // console.log(value);
                      $rootScope.gridOptionscity.data.push( { CityCode: value.cityId, CityName:value.cityName, AirportName:value.airportName, StateId:value.stateRef[0].stateId});
                  })
                
            })
    
    
            $scope.city = [
            {  CityCode:"C100",CityName:"Delhi",AirportName: "Delhi",StateId:40},
            {  CityCode:"C200",CityName:"Bangalore",AirportName: "Bangalore",StateId:50},
            {  CityCode:"C300",CityName:"Patna",AirportName: "Patna",StateId:60},
            {  CityCode:"C400",CityName:"Dehradun",AirportName: "Dehradun",StateId:40 },
            {  CityCode:"C500",CityName:"Mumbai",AirportName: "Mumbai",StateId:40 }
            ];
            $scope.getTableHeight = function() {
               var rowHeight = 60; // your row height
               var headerHeight = 60; // your header height
               return {
                  height: (($rootScope.gridOptionscity.data.length+1) * rowHeight + headerHeight) + "px"
               };
            };
           // $rootScope.gridOptionscity.data = $scope.city;
          
}])
adminModule.directive('lazyModel', function($parse, $compile) {
  return {
    restrict: 'A',  
    require: '^form',
    scope: true,
    compile: function compile(elem, attr) {
        // getter and setter for original model
        var ngModelGet = $parse(attr.lazyModel);
        var ngModelSet = ngModelGet.assign;  
        // set ng-model to buffer in isolate scope
        elem.attr('ng-model', 'buffer');
        // remove lazy-model attribute to exclude recursion
        elem.removeAttr("lazy-model");
        return function postLink(scope, elem, attr) {
          // initialize buffer value as copy of original model 
          scope.buffer = ngModelGet(scope.$parent);
          // compile element with ng-model directive poining to buffer value   
          $compile(elem)(scope);
          // bind form submit to write back final value from buffer
          var form = elem.parent();
          while(form[0].tagName !== 'FORM') {
            form = form.parent();
          }
          form.bind('submit', function() {
            scope.$apply(function() {
                ngModelSet(scope.$parent, scope.buffer);
            });
         });
         form.bind('reset', function(e) {
            e.preventDefault();
            scope.$apply(function() {
                scope.buffer = ngModelGet(scope.$parent);
            });
         });
        };  
     }
  };
});
adminModule.controller('ModalCtrl3',['$scope','$mdDialog','$rootScope',function($scope,$mdDialog,$rootScope)
{
    $scope.open=function(e,row)
    {
       // console.log(row.entity);

        //in here, you can access the event object and row object
        var myEvent = e;
       $scope.currRow = row.entity;
   

            $mdDialog.show({
                controller: DialogController,
                templateUrl: 'edit-modal-city.html',
                parent: angular.element(document.body),
                targetEvent: e,
                clickOutsideToClose:true,
                fullscreen: $scope.customFullscreen,
                resolve: {
                    data: function () {
                        return row.entity;
                    }
                }
            })
                .then(function(answer) {
                    $scope.status = 'You said the information was "' + answer + '".';
                }, function() {
                    $scope.status = 'You cancelled the dialog.';
                })

    }
       
            $scope.showConfirm = function(event,row) {
               var confirm = $mdDialog.confirm()
                  .title('Are you sure to delete the record?')
                  .textContent('Record will be deleted permanently.')
                  .targetEvent(event)
                  .ok('Yes')
                  .cancel('No');
                  $mdDialog.show(confirm).then(function() {
                     console.log(row.entity );
                     var index = $rootScope.gridOptionscity.data.indexOf(row.entity);
                    $rootScope.gridOptionscity.data.splice(index, 1);
                     }, function() {
                        console.log('You decided to keep your record.');
                  });
            };
}])
function DialogController($scope, $mdDialog,data) {


      $scope.currency=data;

    $scope.hide = function() {
        $mdDialog.hide();
    };

    $scope.cancel = function() {
       // $scope.currency=data;
        $mdDialog.cancel();
    };

    $scope.answer = function(answer) {
        $mdDialog.hide(answer);
    };
}
