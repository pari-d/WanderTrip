
var serviceModule=angular.module('ServiceModule',[]);
serviceModule.service('distanceService',['$http',function($http){

  var  displayDistance=function(origin,destination)
    {
        console.log(origin,destination);


        return $http({

            method:'GET',
            dataType:"json",
            headers:{
                'Authorization': undefined,
                "Accept":"application/json, text/plain, */*"
            },

            url:"https://maps.googleapis.com/maps/api/directions/json?origin="+origin+"&destination="+destination+"&key=AIzaSyCTy72FoUHkpqnddgwzmW0eslzOA4gcSP0"

        }).then(function(response){
             console.log(response)
            return response;

        })

    }

    return{
        distanceServiceObject:displayDistance
    }
}])




serviceModule.service('getDataService',['$http','$rootScope',function($http,$rootScope){
  var getList=function(restApi)
   {

       return $http( //2nd return
       {
           method:'GET',
           dataType:'json',
           headers:{
                       'Content-Type':'application/x-www-form-urlencoded'
                   },
           url:'http://localhost:3000/'+restApi
       }).then(function(response) {
           return response; //1st return
       })
   }
   return {//3rd return
   	serviceObj :getList

   }
}])
//all the returns will return the same thing

serviceModule.service('setDataService',['$http',function($http){
      var addStateInfo=function(obj, restApi)
       {
           console.log(obj);
           return $http(
           {
               method:'POST',
               dataType:'json',
               params:obj,
               headers:{
                           'Content-Type':'application/json'  //don't know
                       },
                url:'http://localhost:3000/add/'+restApi
           }).then(function(response){
               console.log(response);
               return response;
               })
       }
       return {
       	serviceObj :addStateInfo
       }
}])
