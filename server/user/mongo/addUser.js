var mongoose=require('../../resources//node_modules/mongoose');
var Register=require('./regSchema').Register;
var config=require('../../config/config')
var options=
{
    user:config.user,
    pass:config.password,
    auth:{
        authdb:config.authdb
    }
}
mongoose.connect(config.url,config.mongodb,config.mongoport,options);


module.exports.Add=function(name,email,password)
{
    var db=mongoose.connection;
    db.once('open',function(){
    });

    var obj=new Register({

        name:name,
        email:email,
        password:password,
        address:"",
        city:"",
        state:"",
        country:"",
        postalCode:"",
        firstName:"",
        lastName:"",
        contactNumber:"",
        DOB:"",
        gender:"",
        nationality:"",
        role:"Admin",
        
    })
    obj.save(function(err,result){
        if(!err)
            console.log(result);
    })
}


module.exports.get=function()
{
    var db=mongoose.connection;
    db.once('open',function(){
    });
    data=Register.find({}).exec(function(err,obj){
        if(!err)
            return(obj);
        return (err);
    });
    return data;
}
