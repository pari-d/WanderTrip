var mongoose=require('../../resources/node_modules/mongoose');
//mongoose.connect('mongodb://localhost:27017/wonderlustdb');

/*var db=mongoose.connection;
db.once('open',function(){
console.log('connected....');*/

//1.Create schema
var stateSchema=new mongoose.Schema(
{
  stateId:{type:Number,
           unique:true
           
          },
  stateName:String
 

});


var citySchema=new mongoose.Schema(
{
  cityId:{type:Number,
          unique:true
        
         },
  airportName:String,
  cityName:String,
  stateId:[{type:mongoose.Schema.Types.ObjectId,ref:'StateModel'}]

});
var passengerSchema=new mongoose.Schema(
{
  pId:{type:Number,
       unique:true
       
       },
  name:String,
  age:Number,
  gender:{
          type:String,
          enum:['Male','Female']
         },
 bookingId:[{type:mongoose.Schema.Types.ObjectId,ref:'BookModel'}]
 

});

var pnrSchema=new mongoose.Schema(
{
 pnrNum:{
          type:String,
          unique:true
          
         },
 isActive:Boolean


});



module.exports.PnrModel=mongoose.model('PnrModel',pnrSchema);

module.exports.StateModel=mongoose.model('StateModel',stateSchema);
module.exports.CityModel=mongoose.model('CityModel',citySchema);
module.exports.PassengerModel=mongoose.model('PassengerModel',passengerSchema);

module.exports.PnrModel=mongoose.model('PnrModel',pnrSchema);
