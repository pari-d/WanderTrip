var mongoose =require('../../resources//node_modules/mongoose');
//request.query.email,request.query.name,request.query.password
var regSchema=new mongoose.Schema({
    name:String,
    email:String,
    password:String,
    address:String,
    city:String,
    state:String,
    country:String,
    postalCode:Number,
    firstName:String,
    lastName:String,
    contactNumber:Number,
    DOB:Date,
    gender:String,
    nationality:String,
    role:String

})


var Register=mongoose.model('Register',regSchema);

//mongoose.connect('mongodb://localhost:27017/happy');

module.exports.Register=mongoose.model('Register',regSchema);