
var mongoose=require('../../resources/node_modules/mongoose');
var config=require('../../config/config')
var options=
{
    user:config.user,
    pass:config.password,
    auth:{
        authdb:config.authdb
    }
}

mongoose.connect(config.url,config.mongodb,config.mongoport,options);

//mongoose.connect('mongodb://localhost:27017/wonderlustdb');

/*var db=mongoose.connection;
db.once('open',function(){
console.log('connected....');*/

//1.Create schema


var airlineSchema = new mongoose.Schema({
 airlineId: String,
 name: String,
 logo: String
})

var stateSchema=new mongoose.Schema(
{
  stateId:{type:Number,
           unique:true
           
          },
  stateName:String
 

});


var citySchema=new mongoose.Schema(
{
  cityId:{type:Number,
          unique:true
        
         },
  airportName:String,
  cityName:String,
  stateRef:[{type:mongoose.Schema.Types.ObjectId,ref:'StateModel'}]

});
var passengerSchema=new mongoose.Schema(
{
  pId:{type:Number,
       unique:true
       
       },
  name:String,
  age:Number,
  gender:{
          type:String,
          enum:['Male','Female']
         },
 bookingId:[{type:mongoose.Schema.Types.ObjectId,ref:'BookModel'}]
 

});

var routeSchema=new mongoose.Schema({
    routeId:Number,
    fromCity:[{ type:mongoose.Schema.Types.ObjectId , ref:'CityModel'}],
    
    toCity:[{ type:mongoose.Schema.Types.ObjectId, ref:'CityModel'}],
    distance:Number
})




module.exports.RouteModel=mongoose.model('RouteModel',routeSchema);
module.exports.AirlineModel=mongoose.model('AirlineModel',airlineSchema)
module.exports.StateModel=mongoose.model('StateModel',stateSchema);
module.exports.CityModel=mongoose.model('CityModel',citySchema);
module.exports.PassengerModel=mongoose.model('PassengerModel',passengerSchema);


