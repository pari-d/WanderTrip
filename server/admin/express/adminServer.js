var express=require("../../resources/node_modules/express");
/*
var fs=require('../../resources/node_modules/fs');//for secured connection-https
var https=require('../../resources/node_modules/https');//for secured connection-https
*/
var bodyParser=require("../../resources/node_modules/body-parser");
var config=require("../../config/config");

var refViewAirline=require("../mongo/viewairline");
var refViewCity=require("../mongo/viewcities");
var refViewState=require("../mongo/viewstates");
var refViewRoute=require("../mongo/viewroutes");
//var MongoClient = require('mongodb').MongoClient;
//var url = 'mongodb://localhost/wonderlustdb';
var cors=require("../../resources/node_modules/cors");
var app=express();
//var str = "";
app.use(cors());
//app.use()
//app.use(bodyParser.json({type:'application/*+json'}))
//app.use(bodyParser.json({type:'application/*+json'}))//In content type:application/anyhing jason
app.use(bodyParser.json())//Fro securd connection not required
app.get("/",function(request,response){
    response.send("Testing Express....");
});

app.get('/airline',function(request,response)//URL Path where my express is running
{
    /*
    response.setHeader('Access-Control-Allow-Origin','*');//* means allow all
    response.setHeader('Access-Control-Allow-Methods','GET','POST');//DeLETE,PUT
    response.setHeader('Access-Control-Allow-Headers','X-Requested-With,Content-Type');//Tells in which format i am sendingdata
    response.setHeader('Access-Control-Allow-Credentials',true);//To allow Credentials
   
    
        MongoClient.connect(url, function(err, db) {
            var cursor = db.collection('statemodels').find();

            cursor.each(function(err, item) {

                if (item != null) {
                    str = str + "    state Name  " + item.stateName + "Stateid:"+item.stateId+"</br>";
                }
            });
            response.send(str);

        })*/
      data=refViewAirline.view();
     data.then(function(res){
     //console.log(res)
    response.send(res);
   })
})
app.get('/city',function(request,response)//URL Path where my express is running
{
      data=refViewCity.view();
      data.then(function(res){
         /* for(pos in res){
              console.log(res[pos])
          }*/
          response.send(res);
      });
     
});
app.get('/state',function(request,response)//URL Path where my express is running
{
      data=refViewState.view();
      data.then(function(res){
         /* for(pos in res){
              console.log(res[pos])
          }*/
          response.send(res);
      });
     
});
app.get('/route',function(request,response)//URL Path where my express is running
{
      data=refViewRoute.view();
      data.then(function(res){
         /* for(pos in res){
              console.log(res[pos])
          }*/
          response.send(res);
      });
     
});

app.post('/add/airline',function(request,response){
  /*  
    response.setHeader('Access-Control-Allow-Origin','*');
    response.setHeader('Access-Control-Allow-Methods','GET','POST');
    response.header('Access-Control-Allow-Headers','X-Requested-With,Content-Type');
    response.setHeader('Access-Control-Allow-Credentials',true);
*/    
    data=refViewAirline.add(request.query.Code,request.query.Name,request.query.logo)
    data.then(function(res){
        response.send(res)
   })
    
})





//app.set('port',3000)//Port no on which listening happens
app.set('port',3000);

app.listen(app.get('port'),function()//For http
{
    console.log("Server started at..."+app.get('port'));
})